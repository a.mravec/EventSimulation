﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventSimulation.Core;
using EventSimulation.Generators;

namespace EventSimulation.Simulation
{
    public class Simulation : ASimulation<Replication>
    {
        public delegate void UpdateResultsHandler(Simulation s);
        public event UpdateResultsHandler UpdateResults;

        public int NumberOfBus { get; set; }
        public int NumberOfServieces { get; set; }
        public int Id { get; set; }

        private GExponential TerminalFirstExponential { get; set; }
        private GExponential TerminalSecondExponential { get; set; }
        private GUniform ServiceUniform { get; set; }
        private GUniform BoardingUniform { get; set; }
        private GUniform GetOffUniform { get; set; }
        private Random Rand { get; set; } = new Random();

        public Statistics.Statistics TimeInSystem { get; set; } = new Statistics.Statistics(true);

        public Simulation(int replications) : base(replications)
        {
            EndTime = 30 * 24 * 60 * 60; // 30 days in seconds

            TerminalFirstExponential = new GExponential(3600.0 / 43.0, Rand.Next());
            TerminalSecondExponential = new GExponential(3600.0 / 19.0, Rand.Next());
            ServiceUniform = new GUniform(2 * 60, 10 * 60, Rand.Next());
            BoardingUniform = new GUniform(10, 14, Rand.Next());
            GetOffUniform = new GUniform(4, 12, Rand.Next());
        }

        protected override void InitializationReplication()
        {
            Replication.TerminalFirstExponential = TerminalFirstExponential;
            Replication.TerminalSecondExponential = TerminalSecondExponential;
            Replication.ServiceUniform = ServiceUniform;
            Replication.BoardingUniform = BoardingUniform;
            Replication.GetOffUniform = GetOffUniform;
            
            Replication.EndReplication += (replication, e) =>
            {
                RepEventArgs repEventArgs = (RepEventArgs)e;
                TimeInSystem.Add(repEventArgs.AverageTimeSystem, 1);

                UpdateResults?.Invoke(this);
            };
        }

        protected override object GetReplicationData()
        {
            return new ReplicationData() { NumberOfBus = NumberOfBus, NumberOfServieces = NumberOfServieces, TotalNumberOfServieces = NumberOfServieces };
        }
    }
}
