﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventSimulation.Core;

namespace EventSimulation.Simulation.Events
{
    class ArrivalTerminalSecondEvent : AEvent
    {
        public ArrivalTerminalSecondEvent(Replication replication) : base(replication)
        {
        }

        public override void Make()
        {
            Replication rep = (Replication)Replication;
            rep.SetCurrentTime(Time);

            Person person = new Person() { StartTime = Time };
            rep.TerminalSecondQueueEnqueue(person);

            if (rep.EndTime > Time)
            {
                rep.AddEvent(GetNextEvent());
            }
        }

        public override AEvent GetNextEvent()
        {
            Replication rep = (Replication)Replication;
            return new ArrivalTerminalSecondEvent(rep) { Time = rep.CurrentTime + rep.TerminalSecondExponential.GetNumber() };
        }
    }
}
