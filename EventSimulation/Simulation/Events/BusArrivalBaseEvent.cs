﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventSimulation.Core;

namespace EventSimulation.Simulation.Events
{
    class BusArrivalBaseEvent : AEvent
    {
        public Bus Bus { get; set; }

        public BusArrivalBaseEvent(AReplication replication) : base(replication)
        {
        }

        public override void Make()
        {
            Replication rep = (Replication)Replication;
            rep.SetCurrentTime(Time);

            Bus.UpdateStatistics(Time);
            rep.UpdateBusStatistics(Bus.UtilizationStatistics.GetAverage());

            rep.AddEvent(GetNextEvent());
        }

        public override AEvent GetNextEvent()
        {
            Replication rep = (Replication)Replication;
            if (Bus.Persons.Count > 0)
            {
                return new GetOffBaseEvent(rep) {Bus = Bus, Time = rep.CurrentTime + Bus.GetOffUniform.GetNumber() };
            }

            if (rep.TerminalFirstQueue.Count + rep.TerminalSecondQueue.Count > 0 || rep.EndTime > Time)
            {
                return new BusArrivalTerminalFirstEvent(rep) { Bus = Bus, Time = rep.CurrentTime + rep.GetTimeFromDistance(rep.BaseToTerminalFirst) };
            }

            return null;
        }
    }
}
