﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EventSimulation.Core;

namespace EventSimulation.Simulation.Events
{
    public class WaitEvent : AEvent
    {
        public int Sleep { get; set; }

        public WaitEvent(AReplication replication) : base(replication)
        {
        }

        public override void Make()
        {
            Replication rep = (Replication)Replication;
            rep.SetCurrentTime(Time);

            Thread.Sleep(Sleep);
        }

        public override AEvent GetNextEvent()
        {
            throw new NotImplementedException();
        }
    }
}
