﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventSimulation.Core;

namespace EventSimulation.Simulation.Events
{
    class PersonWaitBaseEvent : AEvent
    {
        public Person Person { get; set; }

        public PersonWaitBaseEvent(AReplication replication) : base(replication)
        {
        }

        public override void Make()
        {
            Replication rep = (Replication)Replication;
            rep.SetCurrentTime(Time);
            Person.StartTimeInBase = Time;

            if (rep.GetData().NumberOfServieces > 0)
            {
                rep.BaseTimeStatistics.Add(0, Time);
                rep.UpdateServiceStatistics(rep.GetData().NumberOfServieces);
                rep.GetData().NumberOfServieces--;
                rep.AddEvent(GetNextEvent());
            }
            else
            {
                rep.BaseQueueEnqueue(Person);
            }
        }

        public override AEvent GetNextEvent()
        {
            Replication rep = (Replication)Replication;
            return new ServiceEvent(rep) {Person = Person, Time = rep.CurrentTime + rep.ServiceUniform.GetNumber()};
        }
    }
}
