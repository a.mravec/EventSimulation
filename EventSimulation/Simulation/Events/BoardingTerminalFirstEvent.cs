﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventSimulation.Core;

namespace EventSimulation.Simulation.Events
{
    class BoardingTerminalFirstEvent : AEvent
    {
        public Bus Bus { get; set; }

        public BoardingTerminalFirstEvent(AReplication replication) : base(replication)
        {
        }

        public override void Make()
        {
            Replication rep = (Replication)Replication;
            rep.SetCurrentTime(Time);

            rep.BoardingPersonTerminFirst--;
            Bus.PersonsEnqueue(rep.TerminalFirstQueueDequeue());

            rep.AddEvent(GetNextEvent());
        }

        public override AEvent GetNextEvent()
        {
            Replication rep = (Replication)Replication;
            if (rep.TerminalFirstQueue.Count - rep.BoardingPersonTerminFirst > 0 && Bus.Persons.Count < rep.MaxCapacityBus)
            {
                rep.BoardingPersonTerminFirst++;
                return new BoardingTerminalFirstEvent(rep) { Bus = Bus, Time = rep.CurrentTime + Bus.BoardingUniform.GetNumber() };
            }
            
            return new BusArrivalTerminalSecondEvent(rep) { Bus = Bus, Time = rep.CurrentTime + rep.GetTimeFromDistance(rep.TerminalFirstToTerminalSecond) };
        }
    }
}
