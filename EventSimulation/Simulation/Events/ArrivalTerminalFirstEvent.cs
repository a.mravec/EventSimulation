﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventSimulation.Core;

namespace EventSimulation.Simulation.Events
{
    class ArrivalTerminalFirstEvent : AEvent
    {
        public ArrivalTerminalFirstEvent(Replication replication) : base(replication)
        {
        }

        public override void Make()
        {
            Replication rep = (Replication)Replication;
            rep.SetCurrentTime(Time);

            Person person = new Person(){StartTime = Time};
            rep.TerminalFirstQueueEnqueue(person);

            if (rep.EndTime > Time)
            {
                rep.AddEvent(GetNextEvent());
            }
        }

        public override AEvent GetNextEvent()
        {
            Replication rep = (Replication)Replication;
            return new ArrivalTerminalFirstEvent(rep) {Time = rep.CurrentTime + rep.TerminalFirstExponential.GetNumber()};
        }
    }
}
