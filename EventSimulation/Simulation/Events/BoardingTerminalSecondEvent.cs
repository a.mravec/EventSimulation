﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventSimulation.Core;

namespace EventSimulation.Simulation.Events
{
    class BoardingTerminalSecondEvent : AEvent
    {
        public Bus Bus { get; set; }

        public BoardingTerminalSecondEvent(AReplication replication) : base(replication)
        {
        }

        public override void Make()
        {
            Replication rep = (Replication)Replication;
            rep.SetCurrentTime(Time);

            rep.BoardingPersonTerminSecond--;
            Bus.PersonsEnqueue(rep.TerminalSecondQueueDequeue());

            rep.AddEvent(GetNextEvent());
        }

        public override AEvent GetNextEvent()
        {
            Replication rep = (Replication)Replication;
            if (rep.TerminalSecondQueue.Count - rep.BoardingPersonTerminSecond > 0 && Bus.Persons.Count < rep.MaxCapacityBus)
            {
                rep.BoardingPersonTerminSecond++;
                return new BoardingTerminalSecondEvent(rep) { Bus = Bus, Time = rep.CurrentTime + Bus.BoardingUniform.GetNumber() }; ;
            }

            return new BusArrivalBaseEvent(rep) { Bus = Bus, Time = rep.CurrentTime + rep.GetTimeFromDistance(rep.TerminalSecondToBase) };
        }
    }
}
