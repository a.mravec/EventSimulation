﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventSimulation.Core;

namespace EventSimulation.Simulation.Events
{
    class ServiceEvent : AEvent
    {
        public Person Person { get; set; }

        public ServiceEvent(AReplication replication) : base(replication)
        {
        }

        public override void Make()
        {
            Replication rep = (Replication)Replication;
            rep.SetCurrentTime(Time);

            rep.UpdateServiceStatistics(rep.GetData().NumberOfServieces);
            if (rep.BaseQueue.Count > 0)
            {
                rep.AddEvent(GetNextEvent());
            }
            else
            {
                rep.GetData().NumberOfServieces++;
            }

            //END
            rep.PersonGetOutFromSystem(Person);
        }

        public override AEvent GetNextEvent()
        {
            Replication rep = (Replication)Replication;
            return new ServiceEvent(rep) {Person = rep.BaseQueueDequeue(), Time = rep.CurrentTime + rep.ServiceUniform.GetNumber()};
        }
    }
}
