﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventSimulation.Generators;

namespace EventSimulation.Simulation
{
    public class Bus
    {
        public Queue<Person> Persons { get; set; }
        public int MaxCapacity { get; set; }

        public GUniform BoardingUniform { get; set; }
        public GUniform GetOffUniform { get; set; }
        public Statistics.Statistics UtilizationStatistics { get; set; } = new Statistics.Statistics(true);

        public Bus(int maxCapacity)
        {
            Persons = new Queue<Person>();
            MaxCapacity = maxCapacity;
        }

        public Person PersonsDequeue()
        {
            Person person = Persons.Dequeue();
            return person;
        }

        public void PersonsEnqueue(Person person)
        {
            Persons.Enqueue(person);
        }

        public void UpdateStatistics(double currentTime)
        {
            UtilizationStatistics.Add(Persons.Count, currentTime);
        }
    }
}
