﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventSimulation.Simulation.Statistics
{
    public class Statistics
    {
        public Average Average { get; set; } = new Average();
        public Average AverageQuadrant { get; set; } = new Average();
        public WeightAverage WeightAverage { get; set; } = new WeightAverage();
        public double MaxValue { get; set; } = 0;
        public double MinValue { get; set; } = double.MaxValue;
        public bool IsAverage { get; set; }

        public Statistics(bool isAverage)
        {
            IsAverage = isAverage;
        }

        public void Reset()
        {
            MaxValue = 0;
            MinValue = double.MaxValue;
            Average.Reset();
            WeightAverage.Reset();
        }

        public double GetAverage()
        {
            return IsAverage ? Average.CurrentAverage : WeightAverage.CurrentAverage;
        }

        public void Add(double value, double currentTime)
        {
            if (MaxValue < value)
            {
                MaxValue = value;
            }

            if (MinValue > value)
            {
                MinValue = value;
            }

            if (IsAverage)
            {
                Average.Add(value);
                AverageQuadrant.Add(value * value);
            }
            else
            {
                WeightAverage.Add(value, currentTime);
            }
        }

        public double GetS()
        {
            return AverageQuadrant.CurrentAverage  -
                (Average.CurrentAverage  * Average.CurrentAverage);
        }

        public double GetLeftInterval90()
        {
            double s = GetS();

            if (s == 0)
            {
                return 0;
            }

            return Average.CurrentAverage - ((1.645 * Math.Sqrt(s)) / Math.Sqrt(Average.Count - 1));
        }

        public double GetRightInterval90()
        {
            double s = GetS();

            if (s == 0)
            {
                return 0;
            }

            return Average.CurrentAverage + ((1.645 * Math.Sqrt(s)) / Math.Sqrt(Average.Count - 1));
        }

        public double GetLeftInterval95()
        {
            double s = GetS();

            if (s == 0)
            {
                return 0;
            }

            return Average.CurrentAverage - ((1.960 * Math.Sqrt(s)) / Math.Sqrt(Average.Count - 1));
        }

        public double GetRightInterval95()
        {
            double s = GetS();

            if (s == 0)
            {
                return 0;
            }

            return Average.CurrentAverage + ((1.960 * Math.Sqrt(s)) / Math.Sqrt(Average.Count - 1));
        }
    }
}
