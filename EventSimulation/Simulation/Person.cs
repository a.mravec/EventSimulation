﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventSimulation.Simulation
{
    public class Person
    {
        public static int Counter = 1;

        public double StartTime { get; set; }
        public double StartTimeInBase { get; set; }
        public double Id { get; set; }

        public string StartTimeNice
        {
            get
            {
                TimeSpan timeSpan = TimeSpan.FromSeconds(StartTime);
                return timeSpan.ToString(@"hh\:mm\:ss");
            }
        }

        public Person()
        {
            Id = Person.Counter;
            Person.Counter++;
        }
    }
}
