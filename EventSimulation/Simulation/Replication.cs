﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EventSimulation.Core;
using EventSimulation.Generators;
using EventSimulation.Simulation.Events;
using EventSimulation.Simulation.Statistics;
using FibonacciHeap;

namespace EventSimulation.Simulation
{
    public class Replication : AReplication
    {
        public delegate void AddPersonToTerminalFirstQueueHandler(Person person, Statistics.Statistics length, Statistics.Statistics time);
        public event AddPersonToTerminalFirstQueueHandler AddPersonToTerminalFirstQueue;

        public delegate void RemovePersonToTerminalFirstQueueHandler(Person person, Statistics.Statistics length, Statistics.Statistics time);
        public event RemovePersonToTerminalFirstQueueHandler RemovePersonToTerminalFirstQueue;

        public delegate void AddPersonToTerminalSecondQueueHandler(Person person, Statistics.Statistics length, Statistics.Statistics time);
        public event AddPersonToTerminalFirstQueueHandler AddPersonToTerminalSecondQueue;

        public delegate void RemovePersonToTerminalSecondQueueHandler(Person person, Statistics.Statistics length, Statistics.Statistics time);
        public event RemovePersonToTerminalSecondQueueHandler RemovePersonToTerminalSecondQueue;

        public delegate void AddPersonToBaseQueueHandler(Person person, Statistics.Statistics length, Statistics.Statistics time);
        public event AddPersonToBaseQueueHandler AddPersonToBaseQueue;

        public delegate void RemovePersonToBaseQueueHandler(Person person, Statistics.Statistics length, Statistics.Statistics time);
        public event RemovePersonToBaseQueueHandler RemovePersonToBaseQueue;

        public delegate void TimeInSystemHandler(double? time, Statistics.Statistics timeStatistic);
        public event TimeInSystemHandler PersonLeaveSystem;

        public delegate void BusStatisticsHandler(Statistics.Statistics time);
        public event BusStatisticsHandler BusStatisticsUpdate;

        public delegate void ServiceStatisticsHandler(Statistics.Statistics time);
        public event ServiceStatisticsHandler ServiceStatisticsUpdate;

        public delegate void ChartHandler(Dictionary<double,double> data);
        public event ChartHandler ChartUpdate;



        public int MaxCapacityBus { get; set; }
        public double AverageSpeed { get; set; } // km per hour
        public double BaseToTerminalFirst { get; set; }
        public double TerminalFirstToTerminalSecond { get; set; }
        public double TerminalSecondToBase { get; set; }
        public int NumberOfHours { get; set; } = 1;

        public Queue<Person> TerminalFirstQueue { get; set; } = new Queue<Person>();
        public int BoardingPersonTerminFirst { get; set; }
        public Statistics.Statistics TerminalFirstLengthStatistics { get; set; } = new Statistics.Statistics(false);
        public Statistics.Statistics TerminalFirstTimeStatistics { get; set; } = new Statistics.Statistics(true);

        public Queue<Person> TerminalSecondQueue { get; set; } = new Queue<Person>();
        public int BoardingPersonTerminSecond { get; set; }
        public Statistics.Statistics TerminalSecondLengthStatistics { get; set; } = new Statistics.Statistics(false);
        public Statistics.Statistics TerminalSecondTimeStatistics { get; set; } = new Statistics.Statistics(true);

        public Queue<Person> BaseQueue { get; set; } = new Queue<Person>();
        public Statistics.Statistics BaseLengthStatistics { get; set; } = new Statistics.Statistics(false);
        public Statistics.Statistics BaseTimeStatistics { get; set; } = new Statistics.Statistics(true);

        public GExponential TerminalFirstExponential { get; set; }
        public GExponential TerminalSecondExponential { get; set; }
        public GUniform ServiceUniform { get; set; }
        public GUniform BoardingUniform { get; set; }
        public GUniform GetOffUniform { get; set; }

        public Statistics.Statistics TimeInSystemStatistics { get; set; } = new Statistics.Statistics(true);
        public Statistics.Statistics ServiceStatistics { get; set; } = new Statistics.Statistics(false);
        public Statistics.Statistics BusStatistics { get; set; } = new Statistics.Statistics(true);
        public Dictionary<double, double> ChartData { get; set; } = new Dictionary<double, double>();

        public Replication() : base()
        {
            BoardingPersonTerminFirst = 0;
            BoardingPersonTerminSecond = 0;
        }

        public void UpdateStastics()
        {
            BusStatisticsUpdate?.Invoke(BusStatistics);
            ServiceStatisticsUpdate?.Invoke(ServiceStatistics);
            PersonLeaveSystem?.Invoke(CurrentTime, TimeInSystemStatistics);
        }

        public void UpdateBusStatistics(double value)
        {
            BusStatistics.Add(value, CurrentTime);
        }

        public void UpdateServiceStatistics(double value)
        {
            ServiceStatistics.Add((GetData().TotalNumberOfServieces - value), CurrentTime);
        }

        public void PersonGetOutFromSystem(Person person)
        {
            TimeInSystemStatistics.Add(CurrentTime - person.StartTime, CurrentTime);
        }

        public void TerminalFirstQueueEnqueue(Person person)
        {
            TerminalFirstLengthStatistics.Add(TerminalFirstQueue.Count, CurrentTime);
            TerminalFirstQueue.Enqueue(person);
            AddPersonToTerminalFirstQueue?.Invoke(person, TerminalFirstLengthStatistics, TerminalFirstTimeStatistics);
        }

        public Person TerminalFirstQueueDequeue()
        {
            
            TerminalFirstLengthStatistics.Add(TerminalFirstQueue.Count, CurrentTime);
            Person person = TerminalFirstQueue.Dequeue();
            TerminalFirstTimeStatistics.Add(CurrentTime - person.StartTime, CurrentTime);
            RemovePersonToTerminalFirstQueue?.Invoke(person, TerminalFirstLengthStatistics, TerminalFirstTimeStatistics);
            return person;
        }

        public void TerminalSecondQueueEnqueue(Person person)
        {
            TerminalSecondLengthStatistics.Add(TerminalSecondQueue.Count, CurrentTime);
            TerminalSecondQueue.Enqueue(person);
            AddPersonToTerminalSecondQueue?.Invoke(person, TerminalSecondLengthStatistics, TerminalSecondTimeStatistics);
        }

        public Person TerminalSecondQueueDequeue()
        {
            TerminalSecondLengthStatistics.Add(TerminalSecondQueue.Count, CurrentTime);
            Person person = TerminalSecondQueue.Dequeue();
            TerminalSecondTimeStatistics.Add(CurrentTime - person.StartTime, CurrentTime);
            RemovePersonToTerminalSecondQueue?.Invoke(person, TerminalSecondLengthStatistics, TerminalSecondTimeStatistics);
            return person;
        }

        public void BaseQueueEnqueue(Person person)
        {
            BaseLengthStatistics.Add(BaseQueue.Count, CurrentTime);
            BaseQueue.Enqueue(person);
            AddPersonToBaseQueue?.Invoke(person, BaseLengthStatistics, BaseTimeStatistics);
        }

        public Person BaseQueueDequeue()
        {
            BaseLengthStatistics.Add(BaseQueue.Count, CurrentTime);
            Person person = BaseQueue.Dequeue();
            BaseTimeStatistics.Add(CurrentTime - person.StartTimeInBase, CurrentTime);
            RemovePersonToBaseQueue?.Invoke(person, BaseLengthStatistics, BaseTimeStatistics);
            return person;
        }

        protected override void Initialization()
        {
            MaxCapacityBus = 12;
            AverageSpeed = 35.0;
            BaseToTerminalFirst = 6.4;
            TerminalFirstToTerminalSecond = 0.5;
            TerminalSecondToBase = 2.5;

            ArrivalTerminalFirstEvent arrivalTerminalFirstEvent =
                new ArrivalTerminalFirstEvent(this) {Time = CurrentTime + TerminalFirstExponential.GetNumber()};
            ArrivalTerminalSecondEvent arrivalTerminalSecondEvent =
                new ArrivalTerminalSecondEvent(this) { Time = CurrentTime + TerminalSecondExponential.GetNumber() };
            PriorityQueue.Insert(new FibonacciHeapNode<AEvent>(arrivalTerminalFirstEvent, arrivalTerminalFirstEvent.Time));
            PriorityQueue.Insert(new FibonacciHeapNode<AEvent>(arrivalTerminalSecondEvent, arrivalTerminalSecondEvent.Time));

            ReplicationData data = GetData();
            for (int i = 0; i < data.NumberOfBus; i++)
            {
                double timeToTerminalFirst = GetTimeFromDistance(BaseToTerminalFirst);
                BusArrivalTerminalFirstEvent busArrivalTerminalFirstEvent =
                    new BusArrivalTerminalFirstEvent(this)
                    {
                        Time = CurrentTime + timeToTerminalFirst,
                        Bus = new Bus(MaxCapacityBus) { BoardingUniform = BoardingUniform, GetOffUniform = GetOffUniform }
                    };
                PriorityQueue.Insert(new FibonacciHeapNode<AEvent>(busArrivalTerminalFirstEvent, busArrivalTerminalFirstEvent.Time));
            }
        }

        protected override void Tick()
        {
            var minEvent = PriorityQueue.RemoveMin();
            minEvent.Data.Make();

            if (!IsTurbo)
            {
                if (ReplicationSpeed < 9)
                {
                    double minTime = PriorityQueue.Min().Data.Time;
                    if (CurrentTime + 1 < minTime)
                    {
                        int sleep = Convert.ToInt32(Math.Round(ReplicationSpeed, 0));
                        int sleepTime = (9 - sleep) * 100;
                        AddEvent(new WaitEvent(this) { Time = CurrentTime + 1, Sleep = sleepTime });
                    }
                }

                if (CurrentTime > 3 * 3600 * NumberOfHours)
                {
                    NumberOfHours++;
                    UpdateStastics();
                }
            }
            else
            {
                if (CurrentTime > 3 * 3600 * NumberOfHours)
                {
                    NumberOfHours++;
                    ChartData.Add(CurrentTime, TimeInSystemStatistics.GetAverage());
                }
            }
        }

        public override EventArgs ReplicationResult()
        {
            return new RepEventArgs()
            {
                AverageTimeSystem = TimeInSystemStatistics.GetAverage()
            };
        }

        public override void End()
        {
            InvokeEventSetTime();
            ChartUpdate?.Invoke(ChartData);
            BusStatisticsUpdate?.Invoke(BusStatistics);
            ServiceStatisticsUpdate?.Invoke(ServiceStatistics);
            PersonLeaveSystem?.Invoke(null, TimeInSystemStatistics);
            AddPersonToTerminalFirstQueue?.Invoke(null, TerminalFirstLengthStatistics, TerminalFirstTimeStatistics);
            RemovePersonToTerminalFirstQueue?.Invoke(null, TerminalFirstLengthStatistics, TerminalFirstTimeStatistics);
            AddPersonToTerminalSecondQueue?.Invoke(null, TerminalSecondLengthStatistics, TerminalSecondTimeStatistics);
            RemovePersonToTerminalSecondQueue?.Invoke(null, TerminalSecondLengthStatistics, TerminalSecondTimeStatistics);
            AddPersonToBaseQueue?.Invoke(null, BaseLengthStatistics, BaseTimeStatistics);
            RemovePersonToBaseQueue?.Invoke(null, BaseLengthStatistics, BaseTimeStatistics);
        }

        public void AddEvent(AEvent newEvent)
        {
            if (newEvent != null)
            {
                PriorityQueue.Insert(new FibonacciHeapNode<AEvent>(newEvent, newEvent.Time));
            }
        }

        public double GetTimeFromDistance(double distance)
        {
            return (1.0 / AverageSpeed) * distance * 3600;
        }

        public ReplicationData GetData()
        {
            return (ReplicationData) Data;
        }
    }

    public class ReplicationData
    {
        public int NumberOfBus { get; set; }
        public int NumberOfServieces { get; set; }
        public int TotalNumberOfServieces { get; set; }
    }

    public class RepEventArgs : EventArgs
    {
        public double AverageTimeSystem { get; set; }
    }
}
