﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventSimulation.Core;

namespace EventSimulation.Generators
{
    public class GUniform : AGenerator
    {
        public int Min { get; }
        public int Max { get; }

        public GUniform(int min, int max, int seed) : base(seed)
        {
            Min = min;
            Max = max;
        }

        public override double GetNumber()
        {
            return Min + Rand.NextDouble() * (Max - Min);
        }
    }
}
