﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventSimulation.Core;

namespace EventSimulation.Generators
{
    public class GExponential : AGenerator
    {
        public double Median { get; }

        public GExponential(double median, int seed) : base(seed)
        {
            Median = median;
        }

        public override double GetNumber()
        {
            return (- Math.Log(1.0 - Rand.NextDouble(), Math.E)) / (1.0 / Median);
        }
    }
}
