﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EventSimulation.Simulation;
using EventSimulation.Simulation.Statistics;

namespace EventSimulation.Core
{
    public abstract class ASimulation<T> where T: AReplication, new()
    {
        public delegate void FinishSimulationHandler(ASimulation<T> r, EventArgs e);
        public event FinishSimulationHandler FinishSimulation;

        public T Replication { get; set; }
        public int Replications { get; set; }
        public int NumberOfReplication { get; set; }
        protected double EndTime { get; set; }
        protected object ReplicationData { get; set; }
        private ManualResetEvent Mutex { get; }
        private bool IsStoped { get; set; }
        public bool IsTurbo { get; set; }
        public int ReplicationSpeed { get; set; }

        protected ASimulation(int replications)
        {
            Replications = replications;
            NumberOfReplication = 1;
            IsStoped = false;
            Mutex = new ManualResetEvent(true);
            Replication = new T();
        }

        public Task Simulate()
        {
            return Task.Run((async () =>
            {
                for (; NumberOfReplication <= Replications; NumberOfReplication++)
                {
                    Replication = new T();
                    InitializationReplication();
                    Replication.SetData(GetReplicationData());
                    Replication.SetMutex(Mutex);
                    Replication.EndTime = EndTime;
                    Replication.IsTurbo = IsTurbo;

                    await Replication.Replicate();

                    Mutex.WaitOne();

                    if (IsStoped)
                    {
                        break;
                    }
                }
            }));
        }

        public Task SimulateReplication()
        {
            return Task.Run((async () =>
            {
                InitializationReplication();
                Replication.SetData(GetReplicationData());
                Replication.SetMutex(Mutex);
                Replication.EndTime = EndTime;

                await Replication.Replicate();

                Mutex.WaitOne();
                
            }));
        }

        public void Pause()
        {
            Mutex.Reset();
        }

        public void Continue()
        {
            Mutex.Set();
        }

        public void Stop()
        {
            IsStoped = true;
            Replication.IsStoped = true;
            Continue();
        }

        protected abstract void InitializationReplication();
        //protected abstract EventArgs GetEventArgs();
        protected abstract object GetReplicationData();
    }
}
