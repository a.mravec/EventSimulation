﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventSimulation.Core
{
    public abstract class AEvent
    {
        public double Time { get; set; }
        protected AReplication Replication { get; set; }

        protected AEvent(AReplication replication)
        {
            Replication = replication;
        }

        public abstract void Make();
        public abstract AEvent GetNextEvent();
    }
}
