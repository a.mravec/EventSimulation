﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FibonacciHeap;

namespace EventSimulation.Core
{
    public abstract class AReplication
    {
        public delegate void SetCurrentTimeHandler(double time);
        public event SetCurrentTimeHandler SetCurrentTimeEvent;

        public delegate void EndReplicationHandler(AReplication r, EventArgs e);
        public event EndReplicationHandler EndReplication;

        protected FibonacciHeap<AEvent> PriorityQueue { get; set; }

        private ManualResetEvent Mutex { get; set; }
        public bool IsStoped { get; set; }
        public double CurrentTime { get; set; }
        public double EndTime { get; set; }
        public object Data { get; set; }
        public bool IsTurbo { get; set; } = false;
        public double ReplicationSpeed { get; set; } = 0;

        protected AReplication()
        {
            PriorityQueue = new FibonacciHeap<AEvent>();
            IsStoped = false;
        }

        public void SetMutex(ManualResetEvent mutex)
        {
            Mutex = mutex;
        }

        public void SetData(object data)
        {
            Data = data;
        }

        public void SetCurrentTime(double time)
        {
            CurrentTime = time;
            InvokeEventSetTime();
        }

        public void InvokeEventSetTime()
        {
            SetCurrentTimeEvent?.Invoke(CurrentTime);
        }

        public Task Replicate()
        {
            return Task.Run((() =>
            {
                Initialization();

                if (IsTurbo)
                {
                    Thread.Sleep(25);
                }

                while (!IsStoped && !PriorityQueue.IsEmpty() /*&& CurrentTime < EndTime*/)
                {
                    Tick();

                    Mutex.WaitOne();
                }

                EndReplication?.Invoke(this, ReplicationResult());
            }));
        }

        public void AddEvent(AEvent newEvent)
        {
            PriorityQueue.Insert(new FibonacciHeapNode<AEvent>(newEvent, newEvent.Time));
        }

        protected abstract void Initialization();
        protected abstract void Tick();
        public abstract EventArgs ReplicationResult();
        public abstract void End();
    }
}

