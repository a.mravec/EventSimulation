﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventSimulation.Core
{
    public abstract class AGenerator
    {
        protected Random Rand { get; set; }

        protected AGenerator(int seed)
        {
            Rand = new Random(seed);
        }

        public abstract double GetNumber();
    }
}
