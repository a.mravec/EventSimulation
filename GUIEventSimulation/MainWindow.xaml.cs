﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using EventSimulation.Simulation;
using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;

namespace GUIEventSimulation
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public SeriesCollection SeriesCollection { get; set; }
        public SeriesCollection SeriesCollectionForExperiments { get; set; }
        public LineSeries LineSeriesAverageTimeInSystem { get; set; }

        public List<Simulation> Simulations { get; set; } = new List<Simulation>();
        public double OneStep { get; set; }
        public Simulation Simulation { get; set; }
        public ObservableCollection<Person> TerminalFirstObservableCollection { get; set; } = new ObservableCollection<Person>();
        public ObservableCollection<Person> TerminalSecondObservableCollection { get; set; } = new ObservableCollection<Person>();
        public ObservableCollection<Person> BaseObservableCollection { get; set; } = new ObservableCollection<Person>();
        public bool IsStoped { get; set; } = false;
        public bool IsTurbo { get; set; } = false;
        public bool IsStarted { get; set; } = false;
        public bool SetResults { get; set; } = false;

        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = this;
            InitReplication();
            InitSimulation();
            RadioButtonFixedBus.IsChecked = true;
            TextBoxToNumberOfBusSimulation.IsEnabled = false;

            LineSeriesAverageTimeInSystem = new LineSeries()
            {
                Title = "Average time in system [min]",
                Fill = Brushes.Transparent,
                PointGeometrySize = 5,
            };

            SeriesCollection = new SeriesCollection()
            {
                LineSeriesAverageTimeInSystem
            };

            SeriesCollectionForExperiments = new SeriesCollection();
        }

        private void InitReplication()
        {
            ButtonStartReplications.IsEnabled = true;
            ButtonPauseReplications.IsEnabled = false;
            ButtonContinueReplications.IsEnabled = false;
            ButtonStopReplications.IsEnabled = false;

            TextBoxNumberOfBusReplications.IsEnabled = true;
            TextBoxNumberOfService.IsEnabled = true;
        }

        private string GetNiceTime(double time, string format = @"hh\:mm\:ss")
        {
            TimeSpan timeSpan = TimeSpan.FromSeconds(time);
            return timeSpan.ToString(format);
        }

        private void RegisterEvents()
        {
            // end replication
            Simulation.Replication.EndReplication += (replication, e) =>
            {
                Dispatcher.Invoke(() =>
                {
                    if (IsTurbo)
                    {
                        IsStarted = false;
                        SetResults = true;
                        Simulation.Replication.End();
                        ButtonPauseReplications.IsEnabled = false;
                        ButtonStopReplications.IsEnabled = true;
                        Window.IsEnabled = true;
                    }
                    else
                    {
                        InitReplication();
                    }
                });
            };

            // time
            Simulation.Replication.SetCurrentTimeEvent += (time) =>
            {
                Dispatcher.Invoke(() =>
                {
                    if ((!IsStoped && !IsTurbo) || SetResults)
                    {
                        LabelSimulationTime.Content = GetNiceTime(time, @"dd\ hh\:mm\:ss");
                    }
                });
            };

            // add person to terminal first queue
            Simulation.Replication.AddPersonToTerminalFirstQueue += (person, length, time) =>
            {
                Dispatcher.Invoke(() =>
                {
                    if ((!IsStoped && !IsTurbo) || SetResults)
                    {
                        if (person != null)
                        {
                            TerminalFirstObservableCollection.Add(person);
                        }
                        LabelLengthTerminalFirst.Content = TerminalFirstObservableCollection.Count.ToString();
                        LabelMaxLengthTerminalFirst.Content = Math.Round(length.MaxValue, 0).ToString();
                        LabelAverageLengthTerminalFirst.Content = length.GetAverage().ToString("F");
                    }
                });
            };

            // remove person from terminal first queue
            Simulation.Replication.RemovePersonToTerminalFirstQueue += (person, length, time) =>
            {
                Dispatcher.Invoke(() =>
                {
                    if ((!IsStoped && !IsTurbo) || SetResults)
                    {
                        if (person != null)
                        {
                            TerminalFirstObservableCollection.Remove(person);
                        }
                        LabelLengthTerminalFirst.Content = TerminalFirstObservableCollection.Count.ToString();
                        LabelMaxLengthTerminalFirst.Content = Math.Round(length.MaxValue, 0).ToString();
                        LabelAverageLengthTerminalFirst.Content = length.GetAverage().ToString("F");
                        LabelMaxTimeTerminalFirst.Content = GetNiceTime(time.MaxValue);
                        LabelAverageTimeTerminalFirst.Content = GetNiceTime(time.GetAverage());
                    }
                });
            };

            // add person to terminal second queue
            Simulation.Replication.AddPersonToTerminalSecondQueue += (person, length, time) =>
            {
                Dispatcher.Invoke(() =>
                {
                    if ((!IsStoped && !IsTurbo) || SetResults)
                    {
                        if (person != null)
                        {
                            TerminalSecondObservableCollection.Add(person);
                        }
                        LabelLengthTerminalSecond.Content = TerminalSecondObservableCollection.Count.ToString();
                        LabelMaxLengthTerminalSecond.Content = Math.Round(length.MaxValue, 0).ToString();
                        LabelAverageLengthTerminalSecond.Content = length.GetAverage().ToString("F");
                    }
                });
            };

            // remove person from terminal second queue
            Simulation.Replication.RemovePersonToTerminalSecondQueue += (person, length, time) =>
            {
                Dispatcher.Invoke(() =>
                {
                    if ((!IsStoped && !IsTurbo) || SetResults)
                    {
                        if (person != null)
                        {
                            TerminalSecondObservableCollection.Remove(person);
                        }
                        LabelLengthTerminalSecond.Content = TerminalSecondObservableCollection.Count.ToString();
                        LabelMaxLengthTerminalSecond.Content = Math.Round(length.MaxValue, 0).ToString();
                        LabelAverageLengthTerminalSecond.Content = length.GetAverage().ToString("F");
                        LabelMaxTimeTerminalSecond.Content = GetNiceTime(time.MaxValue);
                        LabelAverageTimeTerminalSecond.Content = GetNiceTime(time.GetAverage());
                    }
                });
            };

            // add person to base queue
            Simulation.Replication.AddPersonToBaseQueue += (person, length, time) =>
            {
                Dispatcher.Invoke(() =>
                {
                    if ((!IsStoped && !IsTurbo) || SetResults)
                    {
                        if (person != null)
                        {
                            BaseObservableCollection.Add(person);
                        }
                        LabelLengthBase.Content = BaseObservableCollection.Count.ToString();
                        LabelMaxLengthBase.Content = Math.Round(length.MaxValue, 0).ToString();
                        LabelAverageLengthBase.Content = length.GetAverage().ToString("F");
                    }
                });
            };

            // remove person from base queue
            Simulation.Replication.RemovePersonToBaseQueue += (person, length, time) =>
            {
                Dispatcher.Invoke(() =>
                {
                    if ((!IsStoped && !IsTurbo) || SetResults)
                    {
                        if (person != null)
                        {
                            BaseObservableCollection.Remove(person);
                        }
                        LabelLengthBase.Content = BaseObservableCollection.Count.ToString();
                        LabelMaxLengthBase.Content = Math.Round(length.MaxValue, 0).ToString();
                        LabelAverageLengthBase.Content = length.GetAverage().ToString("F");
                        LabelMaxTimeBase.Content = GetNiceTime(time.MaxValue);
                        LabelAverageTimeBase.Content = GetNiceTime(time.GetAverage());
                    }
                });
            };

            // person leave system
            Simulation.Replication.PersonLeaveSystem += (time, timeStatistic) =>
            {
                Dispatcher.Invoke(() =>
                {
                    if ((!IsStoped && !IsTurbo) || SetResults)
                    {
                        LabelMinTimeInSystem.Content = GetNiceTime(timeStatistic.MinValue);
                        LabelMaxTimeInSystem.Content = GetNiceTime(timeStatistic.MaxValue);
                        LabelAverageTimeInSystem.Content = GetNiceTime(timeStatistic.GetAverage());
                        LabelInterval90.Content = "<" + GetNiceTime(timeStatistic.GetLeftInterval90()) + " , " + GetNiceTime(timeStatistic.GetRightInterval90()) + ">";
                        LabelInterval95.Content = "<" + GetNiceTime(timeStatistic.GetLeftInterval95()) + " , " + GetNiceTime(timeStatistic.GetRightInterval95()) + ">";
                        if (time != null)
                        {
                            double timeD = (double) time.Value;
                            LineSeriesAverageTimeInSystem.Values.Add(new ObservablePoint(timeD, timeStatistic.GetAverage() / 60));
                        }
                    }
                });
            };

            // bus statistics
            Simulation.Replication.BusStatisticsUpdate += (time) =>
            {
                Dispatcher.Invoke(() =>
                {
                    if ((!IsStoped && !IsTurbo) || SetResults)
                    {
                        LabelMaxUtilizationBus.Content = time.MaxValue.ToString("F");
                        LabelAverageUtilizationBus.Content = time.GetAverage().ToString("F");
                    }
                });
            };

            // service statistics
            Simulation.Replication.ServiceStatisticsUpdate += (time) =>
            {
                Dispatcher.Invoke(() =>
                {
                    if ((!IsStoped && !IsTurbo) || SetResults)
                    {
                        LabelMaxUtilizationService.Content = time.MaxValue;
                        LabelAverageUtilizationService.Content = time.GetAverage().ToString("F");
                    }
                });
            };

            // chart update at the end of turbo mode
            Simulation.Replication.ChartUpdate += (data) =>
            {
                Dispatcher.Invoke(() =>
                {
                    foreach (var time in data)
                    {
                        LineSeriesAverageTimeInSystem.Values.Add(new ObservablePoint(time.Key, time.Value / 60));
                    }
                });
            };
        }

        private async void ButtonStartReplication_OnClick(object sender, RoutedEventArgs e)
        {
            SetResults = false;
            IsStarted = true;
            IsStoped = false;
            ResetReplication();
            Simulation = new Simulation(1);
            Simulation.NumberOfBus = int.Parse(TextBoxNumberOfBusReplications.Text);
            Simulation.NumberOfServieces = int.Parse(TextBoxNumberOfService.Text);
            Simulation.Replication.ReplicationSpeed = SliderSpeed.Value;
            Simulation.Replication.IsTurbo = IsTurbo;
            if (IsTurbo && IsStarted)
            {
                Window.IsEnabled = false;
            }
            RegisterEvents();

            ButtonStartReplications.IsEnabled = false;
            ButtonPauseReplications.IsEnabled = true;
            ButtonContinueReplications.IsEnabled = false;
            ButtonStopReplications.IsEnabled = true;
            TextBoxNumberOfBusReplications.IsEnabled = false;
            TextBoxNumberOfService.IsEnabled = false;

            await Simulation.SimulateReplication();
        }

        private void ButtonPauseReplication_OnClick(object sender, RoutedEventArgs e)
        {
            ButtonStartReplications.IsEnabled = false;
            ButtonPauseReplications.IsEnabled = false;
            ButtonContinueReplications.IsEnabled = true;
            ButtonStopReplications.IsEnabled = true;

            Simulation.Pause();
        }

        private void ButtonStopReplication_OnClick(object sender, RoutedEventArgs e)
        {
            Window.IsEnabled = true;
            ButtonStartReplications.IsEnabled = true;
            ButtonPauseReplications.IsEnabled = false;
            ButtonContinueReplications.IsEnabled = false;
            ButtonStopReplications.IsEnabled = false;
            TextBoxNumberOfBusReplications.IsEnabled = true;
            TextBoxNumberOfService.IsEnabled = true;

            Simulation.Stop();
            IsStarted = false;
            IsStoped = true;
            Thread.Sleep(50);
            ResetReplication();
        }

        private void ResetReplication()
        {
            LineSeriesAverageTimeInSystem.Values = new ChartValues<ObservablePoint>();

            TerminalFirstObservableCollection.Clear();
            TerminalSecondObservableCollection.Clear();
            BaseObservableCollection.Clear();

            LabelSimulationTime.Content = "00 00:00:00";

            LabelLengthTerminalFirst.Content = "0";
            LabelMaxLengthTerminalFirst.Content = "0";
            LabelAverageLengthTerminalFirst.Content = "0";
            LabelMaxTimeTerminalFirst.Content = "0";
            LabelAverageTimeTerminalFirst.Content = "0";

            LabelLengthTerminalSecond.Content = "0";
            LabelMaxLengthTerminalSecond.Content = "0";
            LabelAverageLengthTerminalSecond.Content = "0";
            LabelMaxTimeTerminalSecond.Content = "0";
            LabelAverageTimeTerminalSecond.Content = "0";

            LabelLengthBase.Content = "0";
            LabelMaxLengthBase.Content = "0";
            LabelAverageLengthBase.Content = "0";
            LabelMaxTimeBase.Content = "0";
            LabelAverageTimeBase.Content = "0";

            LabelMinTimeInSystem.Content = "0";
            LabelMaxTimeInSystem.Content = "0";
            LabelAverageTimeInSystem.Content = "0";

            LabelMaxUtilizationBus.Content = "0";
            LabelAverageUtilizationBus.Content = "0";

            LabelMaxUtilizationService.Content = "0";
            LabelAverageUtilizationService.Content = "0";
        }

        private void ButtonContinueReplication_OnClick(object sender, RoutedEventArgs e)
        {
            ButtonStartReplications.IsEnabled = false;
            ButtonPauseReplications.IsEnabled = true;
            ButtonContinueReplications.IsEnabled = false;
            ButtonStopReplications.IsEnabled = true;

            Simulation.Continue();
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void CheckBoxTurbo_OnClick(object sender, RoutedEventArgs e)
        {
            if (CheckBoxTurbo.IsChecked != null) IsTurbo = (bool) CheckBoxTurbo.IsChecked;
            if (Simulation != null)
            {
                Simulation.Replication.IsTurbo = IsTurbo;
                if (IsTurbo && IsStarted)
                {
                    TerminalFirstObservableCollection.Clear();
                    TerminalSecondObservableCollection.Clear();
                    BaseObservableCollection.Clear();
                    LabelLengthTerminalFirst.Content = "0";
                    LabelLengthTerminalSecond.Content = "0";
                    LabelLengthBase.Content = "0";

                    Window.IsEnabled = false;
                    ButtonPauseReplications.IsEnabled = false;
                    ButtonStopReplications.IsEnabled = false;
                }
            }
        }

        private void SliderSpeed_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (Simulation != null)
            {
                Simulation.Replication.ReplicationSpeed = e.NewValue;
            }
        }

        //############################################################################################################

        private void InitSimulation()
        {
            ButtonStartSimulation.IsEnabled = true;
            ButtonPauseSimulation.IsEnabled = false;
            ButtonContinueSimulation.IsEnabled = false;
            ButtonStopSimulation.IsEnabled = false;

            TextBoxFromNumberOfServiceSimulation.IsEnabled = true;
            TextBoxFromNumberOfBusSimulation.IsEnabled = true;
            if (RadioButtonFixedService.IsChecked.Value)
            {
                TextBoxToNumberOfBusSimulation.IsEnabled = true;
            }
            else
            {
                TextBoxToNumberOfServiceSimulation.IsEnabled = true;
            }
            TextBoxNumberOfReplicationsSimulation.IsEnabled = true;

            RadioButtonFixedBus.IsEnabled = true;
            RadioButtonFixedService.IsEnabled = true;
        }

        private void ButtonStartSimulation_OnClick(object sender, RoutedEventArgs e)
        {
            ResetSimulation();
            Simulations.Clear();
            SeriesCollectionForExperiments.Clear();

            ButtonStartSimulation.IsEnabled = false;
            ButtonContinueSimulation.IsEnabled = false;
            ButtonPauseSimulation.IsEnabled = true;
            ButtonStopSimulation.IsEnabled = false;

            TextBoxFromNumberOfServiceSimulation.IsEnabled = false;
            TextBoxFromNumberOfBusSimulation.IsEnabled = false;
            TextBoxToNumberOfBusSimulation.IsEnabled = false;
            TextBoxToNumberOfServiceSimulation.IsEnabled = false;
            TextBoxNumberOfReplicationsSimulation.IsEnabled = false;

            RadioButtonFixedBus.IsEnabled = false;
            RadioButtonFixedService.IsEnabled = false;

            Experiment();
        }

        private async void Experiment()
        {
            Window.Cursor = Cursors.AppStarting;
            var watch = System.Diagnostics.Stopwatch.StartNew();

            int count = RadioButtonFixedService.IsChecked.Value ? 
                int.Parse(TextBoxToNumberOfBusSimulation.Text) - int.Parse(TextBoxFromNumberOfBusSimulation.Text) 
                : int.Parse(TextBoxToNumberOfServiceSimulation.Text) - int.Parse(TextBoxFromNumberOfServiceSimulation.Text);

            int countreplication = int.Parse(TextBoxNumberOfReplicationsSimulation.Text);
            OneStep = 100.0 / (count * countreplication + countreplication);
            count++;

            SeriesCollectionForExperiments.Add(new ColumnSeries
            {
                Values = new ChartValues<ObservablePoint>(),
            });

            Task[] tasks = new Task[count];

            int bus = int.Parse(TextBoxFromNumberOfBusSimulation.Text);
            int service = int.Parse(TextBoxFromNumberOfServiceSimulation.Text);

            CreateStatisticsPanel(count, bus, service);

            for (int i = 0; i < count; i++)
            {
                Simulation simulation = new Simulation(countreplication);
                simulation.NumberOfBus = bus;
                simulation.NumberOfServieces = service;
                simulation.Id = i;
                simulation.IsTurbo = true;

                RegisterEventsSimulation(simulation);

                tasks[i] = simulation.Simulate();

                Simulations.Add(simulation);

                SeriesCollectionForExperiments[0].Values.Add(new ObservablePoint(simulation.Id, 0));

                if (RadioButtonFixedService.IsChecked.Value) { bus++; } else { service++; }
            }

            await Task.WhenAll(tasks);

            watch.Stop();
            EndExperiment(watch.ElapsedMilliseconds);
        }

        private void CreateStatisticsPanel(int count, int bus, int service)
        {
            for (int i = 0; i < count; i++)
            {
                StackPanel panel = new StackPanel() {Orientation = Orientation.Vertical};
                Label title = new Label() { Content = $"Cars: {bus} and Service: {service}", FontWeight = FontWeights.Bold};

                StackPanel panelMin = new StackPanel() { Orientation = Orientation.Horizontal };
                Label minTitle = new Label() { Content = "Min:" };
                Label min = new Label() { Content = "0" };
                panelMin.Children.Add(minTitle);
                panelMin.Children.Add(min);

                StackPanel panelMax = new StackPanel() { Orientation = Orientation.Horizontal };
                Label maxTitle = new Label() { Content = "Max:" };
                Label max = new Label() { Content = "0" };
                panelMax.Children.Add(maxTitle);
                panelMax.Children.Add(max);

                StackPanel panelAvg = new StackPanel() { Orientation = Orientation.Horizontal };
                Label avgTitle = new Label() { Content = "Average:" };
                Label average = new Label() { Content = "0" };
                panelAvg.Children.Add(avgTitle);
                panelAvg.Children.Add(average);

                StackPanel panelInterval = new StackPanel() { Orientation = Orientation.Horizontal };
                Label intervalTitle = new Label() { Content = "Interval 90:" };
                Label interval = new Label() { Content = "< , >" };
                panelInterval.Children.Add(intervalTitle);
                panelInterval.Children.Add(interval);

                ProgressBar progressBar = new ProgressBar() { Width = 200, Height = 15, Minimum = 0, Maximum = 100, Value = 0};
                panel.Children.Add(title);
                panel.Children.Add(panelMin);
                panel.Children.Add(panelMax);
                panel.Children.Add(panelAvg);
                panel.Children.Add(panelInterval);
                panel.Children.Add(progressBar);
                Statistics.Children.Add(panel);

                if (RadioButtonFixedService.IsChecked.Value)
                {
                    bus++;
                }
                else
                {
                    service++;
                }
            }
        }

        private void EndExperiment(long time)
        {
            Window.Cursor = Cursors.Arrow;
            ButtonPauseSimulation.IsEnabled = false;
            TimeOfExperiment.Content = "Time: " + GetNiceTime((time / 1000));
            ButtonStopSimulation.IsEnabled = true;
        }

        private void RegisterEventsSimulation(Simulation simulationObj)
        {
            simulationObj.UpdateResults += (simulation) =>
            {
                Dispatcher.Invoke(() =>
                {
                    double progress = (double)simulation.NumberOfReplication / simulation.Replications * 100;
                    int x = RadioButtonFixedService.IsChecked.Value ? simulation.NumberOfBus : simulation.NumberOfServieces;
                    ((Label)((StackPanel)((StackPanel)Statistics.Children[simulation.Id]).Children[1]).Children[1]).Content = GetNiceTime(simulation.TimeInSystem.MinValue);
                    ((Label)((StackPanel)((StackPanel)Statistics.Children[simulation.Id]).Children[2]).Children[1]).Content = GetNiceTime(simulation.TimeInSystem.MaxValue);
                    ((Label)((StackPanel)((StackPanel)Statistics.Children[simulation.Id]).Children[3]).Children[1]).Content = GetNiceTime(simulation.TimeInSystem.GetAverage());
                    ((Label)((StackPanel)((StackPanel)Statistics.Children[simulation.Id]).Children[4]).Children[1]).Content
                        = "<" + GetNiceTime(simulation.TimeInSystem.GetLeftInterval90()) + " , " + GetNiceTime(simulation.TimeInSystem.GetRightInterval90()) + ">";
                    ((ProgressBar)((StackPanel)Statistics.Children[simulation.Id]).Children[5]).Value = progress;
                    SeriesCollectionForExperiments[0].Values[simulation.Id] = new ObservablePoint(x, simulation.TimeInSystem.GetAverage() / 60);

                    ProgressBarSimulation.Value += OneStep;
                });
            };
        }

        private void ResetSimulation()
        {
            ProgressBarSimulation.Value = 0;
            Statistics.Children.Clear();
        }

        private void ButtonPauseSimulation_OnClick(object sender, RoutedEventArgs e)
        {
            Window.Cursor = Cursors.Arrow;
            ButtonStartSimulation.IsEnabled = false;
            ButtonContinueSimulation.IsEnabled = true;
            ButtonPauseSimulation.IsEnabled = false;
            ButtonStopSimulation.IsEnabled = true;

            foreach (var simulation in Simulations)
            {
                simulation?.Pause();
            }
        }

        private void ButtonContinueSimulation_OnClick(object sender, RoutedEventArgs e)
        {
            Window.Cursor = Cursors.AppStarting;
            ButtonStartSimulation.IsEnabled = false;
            ButtonContinueSimulation.IsEnabled = false;
            ButtonPauseSimulation.IsEnabled = true;
            ButtonStopSimulation.IsEnabled = false;

            foreach (var simulation in Simulations)
            {
                simulation?.Continue();
            }
        }

        private void ButtonStopSimulation_OnClick(object sender, RoutedEventArgs e)
        {
            Window.Cursor = Cursors.Arrow;
            TimeOfExperiment.Content = "Time: 00:00:00";
            SeriesCollectionForExperiments.Clear();
            InitSimulation();

            foreach (var simulation in Simulations)
            {
                simulation?.Stop();
            }

            ResetSimulation();
        }

        private void RadioButtonFixedBus_Checked(object sender, RoutedEventArgs e)
        {
            TextBoxFromNumberOfServiceSimulation.IsEnabled = true;
            TextBoxToNumberOfServiceSimulation.IsEnabled = true;
            TextBoxFromNumberOfBusSimulation.IsEnabled = true;
            TextBoxToNumberOfBusSimulation.IsEnabled = false;
        }

        private void RadioButtonFixedService_Checked(object sender, RoutedEventArgs e)
        {
            TextBoxFromNumberOfServiceSimulation.IsEnabled = true;
            TextBoxToNumberOfServiceSimulation.IsEnabled = false;
            TextBoxFromNumberOfBusSimulation.IsEnabled = true;
            TextBoxToNumberOfBusSimulation.IsEnabled = true;
        }
    }
}
